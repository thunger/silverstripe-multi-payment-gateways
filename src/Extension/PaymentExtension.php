<?php

namespace Thunger\SilverStripeMultiGateways\Extension;

use SilverStripe\Core\Config\Config;
use SilverStripe\Omnipay\GatewayInfo;
use SilverStripe\ORM\DataExtension;

class PaymentExtension extends DataExtension {

	private static $db = [
		'GatewayIdentifier' => 'Varchar(128)'
	];

	public function onBeforeWrite() {
		// set identifier as we need this later in process
		if (!$this->owner->GatewayIdentifier && $this->owner->Gateway) {
			$Gateway = Config::inst()->get(GatewayInfo::class, $this->owner->Gateway);
			$this->owner->GatewayIdentifier = $Gateway['gatewayidentifier'];
		}
	}

}