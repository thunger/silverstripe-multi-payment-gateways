<?php

namespace Thunger\SilverStripeMultiGateways\Extension;

use SilverStripe\Core\Config\Config;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Forms\CheckboxSetField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Omnipay\GatewayInfo;
use SilverStripe\Omnipay\Model\Payment;
use SilverStripe\ORM\DataExtension;
use TractorCow\Fluent\State\FluentState;

class PaymentGatewayObjectExtension extends DataExtension {

	use Configurable;

	private static $db = [
		'PaymentGateways' => 'Varchar(255)',
	];

	public function updateCMSFields(FieldList $fields) {
		$PaymentGateways = array_keys(Config::inst()->get('PaymentGateways'));
		$PaymentGateways = array_combine($PaymentGateways, $PaymentGateways);
		$fields->removeByName('Groups');
		$fields->addFieldToTab(
			'Root.Payment',
			CheckboxSetField::create(
				'PaymentGateways',
				'Payment Gateways',
				$PaymentGateways
			)
		);
	}

	public function PaymentGatewaySettings() {
		$return = [];
		$PaymentGatewayConfig = $this->owner->PaymentGateways ? json_decode($this->owner->PaymentGateways, true) : null;
		if (is_array($PaymentGatewayConfig)) {
			$ConfigPaymentGateways = Config::inst()->get('PaymentGateways');
			foreach ($PaymentGatewayConfig as $PaymentGatewayName) {
				if (isset($ConfigPaymentGateways[$PaymentGatewayName])) {
					foreach ($ConfigPaymentGateways[$PaymentGatewayName] as $PaymentGateway => $PaymentGatewayData) {
						$PaymentGatewayData['gatewayidentifier'] = $PaymentGatewayName;
						$return[$PaymentGateway] = $PaymentGatewayData;
					}
				}
			}
		}
		return $return;
	}

	public static function set_payment_gateways($PaymentGateways = null) {
		if (is_null($PaymentGateways)) {
			$objects = self::config()->objects;
			foreach ($objects as $class => $method) {
				$object = $class::$method();
				if ($object && $gateways = $object->PaymentGatewaySettings()) {
					$PaymentGateways = $gateways;
				}
			}
		}

		// get the default gateway for locale if none set
		if (
			(!is_array($PaymentGateways) || !count($PaymentGateways))
			&& class_exists(FluentState::class)
			&& $locale = FluentState::singleton()->getLocale()
		) {
			$ConfigPaymentGateways = Config::inst()->get('PaymentGateways');
			foreach ($ConfigPaymentGateways as $PaymentGatewayIdentifier => $ConfigPaymentGateway) {
				foreach ($ConfigPaymentGateway as $PaymentGatewayName => $PaymentGatewayData) {
					if (
						isset($PaymentGatewayData['default_for_locales'])
						&& in_array($locale, $PaymentGatewayData['default_for_locales'])
					) {
						$PaymentGatewayData['gatewayidentifier'] = $PaymentGatewayIdentifier;
						$PaymentGateways = [$PaymentGatewayName => $PaymentGatewayData];
						break;
					}
				}
			}
		}

		if (is_array($PaymentGateways) && count($PaymentGateways)) {
			Config::modify()
				->remove(GatewayInfo::class);

			$AllowedGateways = [];
			foreach ($PaymentGateways as $PaymentGateway => $PaymentGatewaySettings) {
				GatewayInfo::config()->$PaymentGateway = $PaymentGatewaySettings;
				$AllowedGateways[] = $PaymentGateway;
			}
			Config::modify()
				->set(Payment::class, 'allowed_gateways', $AllowedGateways);
		}
		return $PaymentGateways;
	}

}