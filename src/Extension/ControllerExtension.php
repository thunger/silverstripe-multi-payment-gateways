<?php

namespace Thunger\SilverStripeMultiGateways\Extension;

use SilverShop\Cart\ShoppingCartController;
use SilverStripe\CMS\Controllers\ModelAsController;
use SilverStripe\Core\Config\Config;
use SilverStripe\Omnipay\Model\Payment;
use SilverStripe\Omnipay\PaymentGatewayController;
use SilverStripe\ORM\DataExtension;

class ControllerExtension extends DataExtension {

	// set payment gateways for standard requests
	public function onBeforeInit() {
		// the code below needs to be executed early in the process
		if (
			!is_a($this->owner, ModelAsController::class)
			&& !is_a($this->owner, ShoppingCartController::class)
		) {
			return;
		}

		PaymentGatewayObjectExtension::set_payment_gateways();
	}

	// set payment gateways for payment requests
	// this may not be necessary (need to verify) as the gateway will be adjusted just before finishing th payment.
	// make sure no SilverShop\Order::calculate() gets called here (like on Office::get_for_current_order())
	// update: yes, this is necessary :-)
	public function updatePaymentFromRequest($request, $gateway) {
		if (
			!is_a($this->owner, PaymentGatewayController::class)
		) {
			return;
		}

		$identifier = $request->param('Identifier');
		if (
			$payment = Payment::get()
				->filter('Identifier', $identifier)
				->filter('Identifier:not', '')
				->first()
		) {
			$gateways = [];
			$ConfigPaymentGateways = Config::inst()->get('PaymentGateways');
			if (isset($ConfigPaymentGateways[$payment->GatewayIdentifier])) {
				foreach ($ConfigPaymentGateways[$payment->GatewayIdentifier] as $PaymentGateway => $PaymentGatewayData) {
					$PaymentGatewayData['gatewayidentifier'] = $payment->GatewayIdentifier;
					$gateways[$PaymentGateway] = $PaymentGatewayData;
				}
			}

			PaymentGatewayObjectExtension::set_payment_gateways($gateways);
		}
	}
}