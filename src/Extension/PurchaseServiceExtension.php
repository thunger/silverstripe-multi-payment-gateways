<?php

namespace Thunger\SilverStripeMultiGateways\Extension;

use SilverStripe\Core\Config\Config;
use SilverStripe\Omnipay\GatewayInfo;
use SilverStripe\Omnipay\Model\Payment;
use SilverStripe\ORM\DataExtension;

class PurchaseServiceExtension extends DataExtension {


	public function onBeforePurchase(&$gatewaydata) {
		/*if ($Payment = Payment::get()->filter('OrderID', Session::get('OrderID'))->first()) {
			self::adjust_payment_config($Payment, $gatewaydata);
		}*/
	}


	public function onBeforeCompletePurchase(&$gatewaydata) {
		if (
			isset($gatewaydata['transactionId'])
			&& $gatewaydata['transactionId']
			&& $Payment = Payment::get()->filter('Identifier', $gatewaydata['transactionId'])->first()
		) {
			self::adjust_payment_config($Payment, $gatewaydata);
		}
	}


	public function onBeforeRefund(&$gatewaydata) {
		if (
			isset($gatewaydata['transactionReference'])
			&& $gatewaydata['transactionReference']
			&& $Payment = Payment::get()->filter('TransactionReference', $gatewaydata['transactionReference'])->first()
		) {
			self::adjust_payment_config($Payment, $gatewaydata);
		}
	}

	public static function adjust_payment_config($Payment, &$gatewaydata) {
		/*// add order details for onbeforecomplete
		$gatewaydata = array_merge(
			$gatewaydata,
			$Payment->Order()->PaymentParameter()
		);*/

		// adjust transaction id for the second run to paypal
		if(
			isset($gatewaydata['transactionId'])
			&& $gatewaydata['transactionId'] == $Payment->Identifier
			&& $Payment->Gateway == 'PayPal_Express'
		) {
			$gatewaydata['transactionId'] = $Payment->Order()->Reference;
			$Payment->Order()->extend('updateGatewayData', $gatewaydata);
		}

		// adjust payment account as per payment record
		// needed as this is called by the service hence no user is logged in nor we know the locale
		$ConfigPaymentGateways = Config::inst()->get('PaymentGateways');
		if (isset($ConfigPaymentGateways[$Payment->GatewayIdentifier])) {
			foreach ($ConfigPaymentGateways[$Payment->GatewayIdentifier] as $PaymentGateway => $PaymentGatewayData) {
				if ($PaymentGateway == $Payment->Gateway) {
					Config::modify()->set(GatewayInfo::class, $PaymentGateway, $PaymentGatewayData);
				}
			}
		}
	}

}